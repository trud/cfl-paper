BUILD = build/
DOC_NAME = cfl_paper
XML_FILE = ${BUILD}draft-egge-netvc-cfl-01.xml

LATEXMK = latexmk -pdflatex=lualatex -jobname=${BUILD}${DOC_NAME} -pdf

# Targets
all: pdf txt presentation

pdf:
	$(LATEXMK) main.tex

txt:
	kramdown-rfc2629  main.mkd > ${XML_FILE}
	xml2rfc ${XML_FILE}

presentation:
	latexmk -pdflatex=lualatex -jobname=${BUILD}dcc18_cfl -pdf presentation.tex
